const { I } = inject();

module.exports = {
    accessChat: {
        contactBtn: '//android.view.ViewGroup[@content-desc="bottomTab_contact"]',
        bottomTabChat: '//android.view.ViewGroup[@content-desc="bottomTab_chat"]',
        contactsearch: '//android.widget.EditText[@content-desc="contact_search"]',
        prospectsTab: '//android.view.ViewGroup[@content-desc="contact_tab_nvc"]',
        clientsTab: '//android.view.ViewGroup[@content-desc="contact_tab_clients"]',
        teamTab: '//android.view.ViewGroup[@content-desc="contact_tab_teams"]',
        groupTab: '//android.view.ViewGroup[@content-desc="contact_tab_groups"]',
        user: '//android.view.ViewGroup[@content-desc="teams_0_contact_0"]',
        chatBtn: '//android.view.ViewGroup[@content-desc="teams_0_contact_0"]',
        call: '//android.view.ViewGroup[@content-desc="contact_moreMenu"]',
        mess: '//android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup',
        forwardUser: '//android.view.ViewGroup[@content-desc="teams_0_checkbox_0"]/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView',
        nextButton: '//android.view.ViewGroup[@content-desc="contact_confirm"]',
        backToListChat: '//android.view.ViewGroup[@content-desc="chatDetail_back"]',
        profileBackMenu: '//android.view.ViewGroup[@content-desc="profile_backMenu"]',
    },
    chatDetail: {
        chatDetailInput: '//android.widget.EditText[@content-desc="chatDetail_input"]',
        sendMessageButton: '//android.view.ViewGroup[@content-desc="chatDetail_sendMessage"]/android.widget.TextView',
        firstMessageText: '//*[contains(@text,"{0}")]',
        viewDetailText: '//*[@class="android.widget.TextView" and @text="View details"]',
        diliveredText: '//android.widget.TextView[contains(@text,"Delivered")]',
        messageViewDetailText:'//android.view.ViewGroup[@content-desc="conversation_0"]//android.widget.TextView[contains(@text,"{0}")]',
        backViewDetailIcon: '//android.view.ViewGroup[@content-desc="ic_back"]',
    },
    reply: {
        viewReplyText: '//*[@class="android.widget.TextView" and @text="Reply"]',
        replyPrepare: '//android.widget.TextView[@text= "{0}"]//parent::android.view.ViewGroup//preceding-sibling::android.widget.TextView[@text="ngueyn, nhun"]',
        replySuccessMessage: '//android.widget.TextView[@text="{0}"]//preceding-sibling::android.view.ViewGroup[1]//android.widget.TextView[@text="{1}"]',
    },
    forward: {
        viewForwardText: '//*[@class="android.widget.TextView" and @text="Forward"]',
        forwardSuccessMessage: '//android.widget.TextView[@text="{0}"]//parent::android.view.ViewGroup//parent::android.view.ViewGroup//preceding-sibling::android.widget.TextView[@text="Forwarded"]',
        forwardUserInListChats: '//android.view.ViewGroup[@content-desc="conversation_0"]/android.widget.TextView[@text="2, 1"]',
        searchUserForward: '//android.view.ViewGroup[@content-desc="forward_message_cancel"]//parent::android.view.ViewGroup//parent::android.view.ViewGroup//following-sibling::android.view.ViewGroup[1]//android.widget.EditText[@content-desc="contact_search"]',
        teamTabForward: '//android.view.ViewGroup[@content-desc="contact_tab_teams"]/android.view.ViewGroup',
    },
}

