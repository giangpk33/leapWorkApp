Feature: Business rules
  Send message and view detail
  Send message and reply
  Send message and forward

  Scenario: do something
    Given Use qrCode and Login
    When I access messenger
    Then I view detail
    Then I reply message
    When I send message
    Then I forward message
