const { I } = inject();

const commonSteps = require('./commons.js');
const loginMobilePage = require('../pages/login_mobile_page.js')
module.exports = {

  login(qrCode, password, optCode){
    commonSteps.tapToButton(loginMobilePage.login.skipButton);
    this.inputQrCode(qrCode);
    this.inputPassword(password);
    this.inputOPT(optCode);
    commonSteps.seeText(loginMobilePage.login.chatsText);
  },

  inputQrCode(qrCode){
    for(let i = 0; i < qrCode.length; i++)
    {
      commonSteps.fillToFieldDynamic(loginMobilePage.login.codeDynamic, qrCode.charAt(i), i);
    }
  },

  inputPassword(password){
    commonSteps.fillToField(loginMobilePage.login.passWord, password);
    commonSteps.tapToButton(loginMobilePage.login.signUpButton);
  },

  inputOPT(otpCode){
    for(let i = 0; i < otpCode.length; i++){
      commonSteps.fillToFieldDynamic(loginMobilePage.login.otpDynamic, otpCode.charAt(i), i);
    }
  },
}
