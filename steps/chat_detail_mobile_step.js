const { I } = inject();

const chatDetailPage = require('../pages/chat_detail_mobile_page.js')
const loginMobilePage = require('../pages/login_mobile_page.js')
const commonSteps = require('../steps/commons.js')

module.exports = {

    viewDetailMessage(message){
        this.sendNewMessage(message);

        this.tabToViewDetailMessage(message);

        this.verifyMessageDetail(message);
    },

    replyMessage(sentMessage, replyMessage){
        this.tabToReplyMessage(sentMessage);
    
        this.sendNewMessage(replyMessage);

        this.verifyMessageReply(replyMessage, sentMessage);
    },

    forwardMessage(sentMessage, forwardUsername){
        this.tabToForwardMessage(sentMessage);
        
        this.selectUserForward(forwardUsername);

        this.backToListChat();

        this.verifyMessageForward(sentMessage);
    },

    accessChat(userName){
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.accessChat.contactBtn));
        commonSteps.tapToButton(chatDetailPage.accessChat.contactBtn);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.accessChat.prospectsTab));
        commonSteps.tapToButton(chatDetailPage.accessChat.teamTab);
        this.inputsearch(userName);

        commonSteps.tapToButton(chatDetailPage.accessChat.user);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.accessChat.chatBtn));
        this.chat();
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.accessChat.call)); 
    },

    inputsearch(userName){
        commonSteps.fillToField(chatDetailPage.accessChat.contactsearch, userName)
    },

    chat(){
        commonSteps.tapToButton(chatDetailPage.accessChat.chatBtn)
    },

    
    sendNewMessage(message){
        commonSteps.fillToField(chatDetailPage.chatDetail.chatDetailInput, message);
        commonSteps.tapToButton(chatDetailPage.chatDetail.sendMessageButton);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.chatDetail.firstMessageText, message));
    },

    selectUserForward(forwardUsername) {
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.accessChat.prospectsTab));
        commonSteps.touchPerform(660, 510);
        commonSteps.fillToField(chatDetailPage.forward.searchUserForward,forwardUsername);
    
        commonSteps.tapToButton(chatDetailPage.accessChat.forwardUser);
        commonSteps.tapToButton(chatDetailPage.accessChat.nextButton);
    },

    backToListChat(){
        commonSteps.tapToButton(chatDetailPage.accessChat.backToListChat);
        commonSteps.tapToButton(chatDetailPage.accessChat.profileBackMenu);
        commonSteps.tapToButton(chatDetailPage.accessChat.bottomTabChat);
    },

    verifyMessageForward(sentMessage){
        commonSteps.seeText(loginMobilePage.login.chatsText);
        commonSteps.tapToButton(chatDetailPage.forward.forwardUserInListChats);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.forward.forwardSuccessMessage, sentMessage));
    },

    verifyMessageDetail(message){
        commonSteps.seeText(message);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.chatDetail.backViewDetailIcon));
        commonSteps.tapToButton(chatDetailPage.chatDetail.backViewDetailIcon);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.accessChat.call)); 
    },

    verifyMessageReply(replyMessage, sentMessage){
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.reply.replySuccessMessage, replyMessage, sentMessage));
    },

    tabToViewDetailMessage(message){
        commonSteps.longPressDynamicElement(chatDetailPage.chatDetail.firstMessageText, message);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.chatDetail.viewDetailText));
        commonSteps.tapToButton(chatDetailPage.chatDetail.viewDetailText);
    },
    tabToReplyMessage(sentMessage){
        commonSteps.longPressDynamicElement(chatDetailPage.chatDetail.firstMessageText, sentMessage);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.chatDetail.viewDetailText));
        commonSteps.tapToButton(chatDetailPage.reply.viewReplyText);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.reply.replyPrepare, sentMessage));
    },
    tabToForwardMessage(sentMessage){
        commonSteps.longPressDynamicElement(chatDetailPage.chatDetail.firstMessageText, sentMessage);
        commonSteps.verifyTrue(commonSteps.isElementDisplayed(chatDetailPage.chatDetail.viewDetailText));
        commonSteps.tapToButton(chatDetailPage.forward.viewForwardText);
    }

}