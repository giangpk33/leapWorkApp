const { I } = inject();

const loginMobileStep = require('../steps/login_mobile_step.js');
const chatDetailStep = require('../steps/chat_detail_mobile_step.js');

// Add in your custom step files

 
  let qrCode = '1an3abj4tv';
  let password = 'Testing@123';
  let optCode = '111111';
  let sendMsg = 'send msg test 9';
  let replyMsg = 'reply msg test 9';
  let forwardMsg = 'forward msg test 9';
  let forwardUser = 'am.custom.95';
  let sendUser = 'thao.01';

Given('Use qrCode and Login', () => {
  //loginMobileStep.login(qrCode, password, optCode);
});
When('I access messenger', () => {
  chatDetailStep.accessChat(sendUser);
});
Then('I view detail', () => {
  chatDetailStep.viewDetailMessage(sendMsg);
});
Then('I reply message', () => {
  chatDetailStep.replyMessage(sendMsg, replyMsg);
});
When('I send message', () => {
  chatDetailStep.sendNewMessage(forwardMsg);
});
Then('I forward message', () => {
  chatDetailStep.forwardMessage(forwardMsg, forwardUser)
});


